import React, { useState } from "react";
import { Fab, TextareaAutosize } from "@mui/material";
import { Link } from "react-router-dom";
import { QrReader } from "react-qr-reader";
function QRscanner({ setQrscan, handleClose }: any) {
  const handleScan = (data: any, error: any) => {
    if (data) {
      setQrscan(data.text);
      handleClose();
    }

    if (!!error) {
      console.info(error);
    }
  };

  return (
    <div>
      <div>
        <QrReader
          onResult={handleScan}
          className="scanner-root"
          constraints={{ facingMode: "user" }} // delay={300}
          videoContainerStyle={{
            border: "5px solid red",
            height: "100%",
            width: "100%",
          }}
          scanDelay={300}
          containerStyle={{ height: "100%", width: "100%" }}
          videoStyle={{ height: "100%", width: "100%" }}
        />
      </div>
    </div>
  );
}

export default QRscanner;
