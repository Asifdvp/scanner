import TextareaAutosize from "@mui/material/TextareaAutosize";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import React from "react";
import QRscanner from "./pages/Qrscanner";
import Scanner from "./pages/Scanner";
import CustomModal from "./components/UI/CustomModal";

function App() {
  const [qrscan, setQrscan] = React.useState("No result");
  return (
    <div className="App">
      <div className="App-header">
        <CustomModal>
          <Scanner setQrscan={setQrscan} />
        </CustomModal>

        <TextareaAutosize
          style={{ fontSize: 18, width: 320, height: 100, marginTop: 100 }}
          maxRows={4}
          defaultValue={qrscan}
          value={qrscan}
        />
      </div>
    </div>
  );
}

export default App;
